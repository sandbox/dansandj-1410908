CONTENTS OF THIS FILE
---------------------

  * Introduction
  * Installation
  * For Developers
  * Maintainers

INTRODUCTION
------------


INSTALLATION
------------
Roles to Groups can be installed similar to other contributed Drupal modules.  

  1) Place all contents of the "roles_to_groups" folder in your module's directory.
  2) Navigate to the 'admin/build/modules'.
  3) First ensure the "Monster Menus" module is enabled, then enable the "Roles to Groups" module.


FOR DEVELOPERS
--------------


MAINTAINERS
-----------
- nackersa (Drew Nackers)
- jay.dansand (Jay Dansand)
